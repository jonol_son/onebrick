package com.onebrick;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class Intro extends Activity {
	int city, tempcity;
	String sCity = "myCity", shaPrefs = "sh";
	List<String> SpinnerArray = new ArrayList<String>();
	Spinner locations; 
	Button btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		city = 0;  
		read();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.introlayout);
		addLocs();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, SpinnerArray);
		locations = (Spinner) findViewById(R.id.spin);
		locations.setAdapter(adapter);
		locations.setSelection(city);
		//Click listener for the added items
	
			locations.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> adapter2, View arg1,
						int arg2, long arg3) {
				
					tempcity = arg2;
					
				}

				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});
		
		
		btn = (Button) findViewById(R.id.setLoc);
		 btn.setOnClickListener(new OnClickListener()
	        {
	          public void onClick(View v)
	          {
	        	 city = tempcity;
	    		write();
	    		Bundle b = new Bundle();
				try {
					Class ourClass = Class.forName("com.onebrick.Menu");
					Intent in = new Intent(Intro.this, ourClass);
					in.putExtra("city", city);
					startActivity(in);
				}catch (ClassNotFoundException e) {
					e.printStackTrace();	}
	          }
	        });
	}
	private void addLocs() {
		SpinnerArray.add("Boston");
		SpinnerArray.add("Chicago");
		SpinnerArray.add("Detroit");
		SpinnerArray.add("Los Angeles");
		SpinnerArray.add("Minneapolis/St. Paul");
		SpinnerArray.add("New York");
		SpinnerArray.add("Orlando");
		SpinnerArray.add("Philadelphia");
		SpinnerArray.add("San Francisco");
		SpinnerArray.add("Seattle");
		SpinnerArray.add("Silicon Valley");
		SpinnerArray.add("Washington D.C.");
	}

	private void read() {
		SharedPreferences sp = getSharedPreferences(shaPrefs, 0);
		if (sp.contains(sCity))
		{
			Log.d("blarg","blarg" + city+ "");
			city = sp.getInt(sCity, tempcity);	
			Log.d("blarg","blarg" + city + "");
		}
	}

	private void write() {
		SharedPreferences sp = getSharedPreferences(shaPrefs, 0);
		SharedPreferences.Editor ed = sp.edit();
		ed.putInt(sCity, city);
		ed.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.intro, menu);
		return true;
	}
	
	protected void onPause() {
	super.onPause();
	finish();
	}

}
