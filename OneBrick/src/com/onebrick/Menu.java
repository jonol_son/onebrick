package com.onebrick;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class Menu extends Activity {

	int items = 0;
	List<String> SpinnerArray = new ArrayList<String>();
	Spinner locations;
	int city;
	ImageButton ibutton;
	Button btn;
	EditText ev;
	Boolean pathSet;
	String sCity = "myCity", shaPrefs = "sh", cityName, imgPath = "";
	// variable for selection intent
	private final int PICKER = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		Bundle b = getIntent().getExtras();
		pathSet = false;
		int city = b.getInt("city");
		// compensate for difference in spinner positioning
		city++;
		WebView wv = (WebView) findViewById(R.id.webkit);
		wv.setWebViewClient(new MyWebView());
		wv.getSettings().setJavaScriptEnabled(true);
		//wv.setInitialScale(80);
		switch (city) {
		case 1:
			wv.loadUrl("http://boston.onebrick.org/calendar/");
			cityName = "Boston";
			break;
		case 2:
			wv.loadUrl("http://chicago.onebrick.org/calendar/");
			cityName = "Chicago";
			break;
		case 3:
			wv.loadUrl("http://detroit.onebrick.org/calendar/");
			cityName = "Detroit";
			break;
		case 4:
			wv.loadUrl("http://losangeles.onebrick.org/calendar/");
			cityName = "Los Angeles";
			break;
		case 5:
			wv.loadUrl("http://minneapolis.onebrick.org/calendar/");
			cityName = "Minneapolis/St. Paul";
			break;
		case 6:
			wv.loadUrl("http://newyork.onebrick.org/calendar/");
			cityName = "New York";
			break;
		case 7:
			wv.loadUrl("http://orlando.onebrick.org/calendar/");
			cityName = "Orlando";
			break;
		case 8:
			wv.loadUrl("http://philadelphia.onebrick.org/calendar/");
			cityName = "Philadelphia";
			break;
		case 9:
			wv.loadUrl("http://sfbay.onebrick.org/calendar/");
			cityName = "San Francisco";
			break;
		case 10:
			wv.loadUrl("http://seattle.onebrick.org/calendar/");
			cityName = "Seattle";
			break;
		case 11:
			wv.loadUrl("http://siliconvalley.onebrick.org/calendar/");
			cityName = "Silicon Valley";
			break;
		case 12:
			wv.loadUrl("http://washingtondc.onebrick.org/calendar/");
			cityName = "Washington D.C.";
			break;
		}

		// CAMERA VIEW
		ibutton = (ImageButton) findViewById(R.id.ib);

	}

	public void ibutton(View v) {

		// take the user to their chosen image selection app (gallery or file
		// manager)
		Intent pickIntent = new Intent();
		pickIntent.setType("image/*");
		pickIntent.setAction(Intent.ACTION_GET_CONTENT);
		// we will handle the returned data in onActivityResult
		startActivityForResult(
				Intent.createChooser(pickIntent, "Select Picture"), PICKER);
	}

	public void calendar(View v) {
		setContentView(R.layout.menu);
	}

	public void changeloc(View v) {
		// Editor editor =
		// PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
		// editor.putString("loc", "delete");
		// editor.commit();
		SharedPreferences sp = getSharedPreferences(shaPrefs, 0);
		SharedPreferences.Editor ed = sp.edit();
		ed.remove(sCity);
		ed.commit();
		Intent i = new Intent(this, Intro.class);
		startActivity(i);
	}

	public void submitPhoto(View v) {
		setContentView(R.layout.camera);
	}

	public void sendit(View v) {
		ev = (EditText) findViewById(R.id.event);
		Log.d("clicked", "ty" + imgPath);
		String event = ev.getText().toString();
		if (pathSet == true && event.length()>2) {
			String subject = cityName + " " + event, message = "Attached to this e-mail is the picture you selected!";
			
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
					new String[] { "emruddle@gmail.com" });
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			// emailIntent.setType("image/png");
			emailIntent.setType("image/*");
			emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+imgPath));

			startActivity(emailIntent);
			
			Toast t = Toast.makeText(this, "Sending Image...", Toast.LENGTH_SHORT);
			t.show();
			Thread timer = new Thread(){
				public void run(){
					try{
						sleep(2000);
					} catch (InterruptedException e){
						e.printStackTrace();
					}finally{
						//set up new intent using action name from manifest
						
					}
				}
			};
			timer.start();
			Toast d = Toast.makeText(this, "Image has been SENT! successfully", Toast.LENGTH_SHORT);
			d.show();
		} 
		else if (pathSet != true && event.length()>2)
		{
			Toast t = Toast.makeText(this, "Please Choose an image first!",
					Toast.LENGTH_SHORT);
			t.show();
		}
		else
		{
			Toast t = Toast.makeText(this, "Please enter the Event's name!",
					Toast.LENGTH_SHORT);
			t.show();
		}
		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		setContentView(R.layout.camera);
		if (resultCode == RESULT_OK) {
			// check if we are returning from picture selection
			if (requestCode == PICKER) {
				// import the image
				// the returned picture URI
				Uri pickedUri = data.getData();
				// declare the bitmap
				Bitmap pic = null;
				// declare the path string
				// String imgPath = "";
				// retrieve the string using media data
				String[] medData = { MediaStore.Images.Media.DATA };
		
				// query the data
				Cursor picCursor = getContentResolver().query(pickedUri,
						medData, null, null, null);
		
				if (picCursor != null) {
					// get the path string
					int index = picCursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					picCursor.moveToFirst();
					imgPath = picCursor.getString(index);
					Log.d("index", "" + index);
					Log.d("imgPath", "" + imgPath);
				} else {
					imgPath = pickedUri.getPath();
					Log.d("imgPath BBBBBB", "" + imgPath);
				}
				// if we have a new URI attempt to decode the image bitmap
				
				if (pickedUri != null) {
					// set the width and height we want to use as maximum
					// display
					int targetWidth = 600;
					int targetHeight = 400;
					// create bitmap options to calculate and use sample size
					BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
					// first decode image dimensions only - not the image bitmap
					// itself
					bmpOptions.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(imgPath, bmpOptions);
					// image width and height before sampling
					int currHeight = bmpOptions.outHeight;
					int currWidth = bmpOptions.outWidth;
					// variable to store new sample size
					int sampleSize = 1;
					if (currHeight > targetHeight || currWidth > targetWidth) {
						// use either width or height
						if (currWidth > currHeight)
							sampleSize = Math.round((float) currHeight
									/ (float) targetHeight);
						else
							sampleSize = Math.round((float) currWidth
									/ (float) targetWidth);
					}
					// use the new sample size
					bmpOptions.inSampleSize = sampleSize;
					// now decode the bitmap using sample options
					bmpOptions.inJustDecodeBounds = false;
					// get the file as a bitmap
					pic = BitmapFactory.decodeFile(imgPath, bmpOptions);
					ibutton = (ImageButton) findViewById(R.id.ib);
					// display the newly selected image at larger size
					ibutton.setImageBitmap(pic);
					pathSet=true;
					// scale options
					ibutton.setScaleType(ImageView.ScaleType.FIT_CENTER);
					picCursor.close();
				}
			}
		}
		// superclass method
		super.onActivityResult(requestCode, resultCode, data);
	}

}